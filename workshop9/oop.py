"""
OOP - Object Oriented Programming
Modeling real life objects in python
"""
import sys

class Dog:
    name: str
    age: int
    color: str

    def __init__(self, name: str = 'N/A', age: int = 5) -> None:
        # dunder methods with __method_name__ / magic method

        # init -> constructor is responsible to build our object
        self.name = name
        self.age = age
        self.color = 'black'

    def roll(self) -> None:
        print(id(self))

        print(f'{self.name} is rolling on the floor!')

    def describe(self) -> None:
        print(f'{self.name} is {self.age} years - {self.color}')

    def happy_birthday(self) -> None:
        self.age += 1
        print(f'Happy Birthday: {self.name}')


my_dog: Dog = Dog('Barkley', 3)  # instance / object
print(id(my_dog))
friends_dog: Dog = Dog('Miracle', 6)
my_dog.color = 'Blue'
# friends_dog.legs = 4 NOT RECOMMENDED!
shelter: list[Dog] = [
    Dog('N/A'),
    Dog('N/A'),
    Dog('N/A', 1),
    Dog('N/A'),
]

# print(sys.getsizeof('Barkley'), sys.getsizeof(3))
# print(sys.getsizeof(my_dog.name), sys.getsizeof(my_dog.age))
print(my_dog.name, my_dog.age)
print(friends_dog.name, friends_dog.age)
# my_dog.age += 1
my_dog.happy_birthday()
# Dog.happy_birthday(my_dog)

friends_dog.describe()
my_dog.describe()
friends_dog.roll()
my_dog.roll()

for dog in shelter:
    dog.describe()
