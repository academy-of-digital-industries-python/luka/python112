# users: list[tuple[str, str]] = [
#     ('George', '123'),
#     ('Admin', '555')
# ]

# f = open('D:\\Workspace\\Academy\\python112\\workshop9\\db.txt')
with open('D:\\Workspace\\Academy\\python112\\workshop9\\db.txt') as f:
    # content: str = f.read()
    users: dict[str, str] = {}

    for line in f.readlines():
        user, pass_ = line.strip().split(',')
        users[user] = pass_

# f.close()
# print(users)
username: str = input('Enter your username: ')
password: str = input('Enter your password: ')

# # is_logged_in: bool = False
# # for user, pass_ in users:
# #     if user == username and pass_ == password:
# #         is_logged_in = True
# #         break

db_user_pass: str | None = users.get(username)

if db_user_pass is None or db_user_pass != password:
    print('Check again!')
    quit()

print(f'Welcome {username}')
