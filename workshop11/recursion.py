"""
Challenges:
1. Write absolute precision
    print(f'{first_half} * 10 ^ {second_half}')
2. write fibonacci with for or while loop
"""
import sys
from functools import cache

sys.setrecursionlimit(999999)

fib_cache: dict[int, int] = {0: 0, 1: 1}


@cache
def fib(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fib(n - 1) + fib(n - 2)


def F(n: int) -> int:
    result: int | None = fib_cache.get(n)
    if result is not None:
        return result

    result = F(n - 1) + F(n - 2)
    fib_cache[n] = result
    return result


def main() -> None:
    result = str(F(9999))
    print(result)
    first_half = result[:2]
    first_half = f"{first_half[0]}.{first_half[1]}"
    second_half = len(result) - 1
    print(first_half)
    print(f"{first_half} * 10 ^ {second_half}")


if __name__ == "__main__":
    main()
