from transport import Transport


class Car(Transport):
    wheels: int

    def __init__(self, brand: str, year: int, wheels: int) -> None:
        super().__init__(brand, year)
        self.wheels = wheels

    def describe(self) -> str:
        """Method Overriding"""
        description: str = super().describe()
        return f"{description} has {self.wheels} wheels"
