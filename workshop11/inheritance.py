"""
Polymorphism / Inheritance

for naming classes we use PascalCase convention

- Method overriding
- What is Super
- Child / Parent classes
"""
from car import Car
from boat import Boat
from transport import Transport
from plane import Plane


def main() -> None:
    transports: list[Transport] = [
        Car("TeslaMotors", 2023, 6),
        Boat("TeslaBoats", 2023),
        Plane("TeslaPlanes", 2023),
    ]


    for transport in transports:
        print(transport.describe())
        transport.move()

if __name__ == '__main__':
    main()
