class Transport:
    brand: str
    year: int

    def __init__(self, brand: str, year: int) -> None:
        self.brand = brand
        self.year = year

    def describe(self) -> str:
        return f"This is a: {self.brand} - {self.year}"

    def move(self) -> str:
        print(f'{self} - is moving')
