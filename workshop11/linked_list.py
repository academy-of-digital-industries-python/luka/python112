class Node:
    value: int
    next_ = None

    def __init__(self, value: int, next_=None) -> None:
        self.value = value
        self.next_ = next_


n1 = Node(5)
n2 = Node(7)
n3 = Node(8)

n1.next_ = n2
n2.next_ = n3

print(n1.value)
print(n1.next_.value)
print(n1.next_.next_.value)
