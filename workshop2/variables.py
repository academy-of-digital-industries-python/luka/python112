import sys
# Variables

print(5 + 9 + 10)  # evaluation

# declaration
x = 11 # variable
# snake_case:
size_of_x = sys.getsizeof(x)
y = 32  
# be careful
aml1 = 2
amO0 = 1


print(9223372036854775807 + 1)

print('Size of x:', size_of_x, 'Byte')
# 1 byte = 8 bit
print('Size of x:', size_of_x * 8, 'Bit')

print(x)
print(type(x))
print(y)
print((x + y) * x)

print(x + 7)
print(x - 9)
print((x ** 2) - 9)
