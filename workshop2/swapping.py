# assignment
x, y = -98, 1234
x = x + 1

print('x =', x, 'y =', y)
# swap x and y

# reassignment
# x_temp = x # 23
# x = y # 3
# y = x_temp # 23
x, y = y, x
print('x =', x, 'y =', y)
x, y = y, x
print('x =', x, 'y =', y)
