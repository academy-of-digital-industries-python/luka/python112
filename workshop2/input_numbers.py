# type casting
magic_number = float(input('Enter number: '))
# magic_number = int('5')
# print(type(magic_number))
# print(magic_number + 29)
# cast to string
# print(str(magic_number) + '5')

# assume magic_number is 75     .5
# we want add 5 into the end of the whole number 755.5

# 1. with type casting
whole = int(magic_number)
fraction = magic_number - whole
whole = int(f'{whole}5')
# print(whole)
# print(fraction)
print(f'Task 2: {whole + fraction}')

