x = 7

# new line symbol
print('hello\nthere')
print('Hi there\n\n\n')
# tab indentaion
print('Hi\tthere')
print('\\' * 2)
print('\'Hello There\'')
print("'Helo There'")
print("\"Helo There\"")
print(""""Helo There\"""")
print('''
I'm 
      multi
      line
	string
''')

milion = 1_000_000
print(milion)

