""" Multiline string / Docstring
String methods:
- strip
- rstrip
- lstrip
- upper
- lower
- capitalize
- title

"""
# int | integer
my_secret_number = 8

print(my_secret_number, type(my_secret_number))

# float
pi = 3.14
print(pi, type(pi))

# str | string

first_name = 'john jake  '
last_name = '   Doe'

first_name = first_name.rstrip().capitalize()

last_name = last_name.lstrip().upper()
age = 32
print(first_name.title(), type(first_name))
# Concatenation
# full_name = first_name + ' ' + last_name
full_name = f'{first_name} {last_name} and my age is {age}'

print(full_name)