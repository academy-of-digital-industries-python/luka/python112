# multiline strings

string_1 = """
This is a multiline string
"""
string_2 = '''
this is another multiline string
'''

# just strings
name = 'Luka'
name_2 = "Gio"


# escape characters / special characters

tabs = '\t\t\tIndented by 3 tabs'
print(tabs)
quoted_string = 'john\'s car'
print(quoted_string)

string_with_backslash = '\\'
print(string_with_backslash)

