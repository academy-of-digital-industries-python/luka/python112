# Data Structure: List (Array)

empty_list = []
print(empty_list)

# indexed from 0 to n (n is a number of elements - 1)
# in this case we have 5 elements and the last element's index is 4
even_numbers = [2, 4, 8, 10, 6]
print(even_numbers, type(even_numbers))

# access
print(even_numbers[0])
print(even_numbers[4])

print('\nFirst Names list:')
first_names = [
    'Jane', 'Josh', 'John', 'Mary'
]
print(first_names[0])
print(first_names[3])

# appending to list (Adding new elements)

even_numbers.append(28)
even_numbers.append(30)
even_numbers.append(12)
even_numbers.append(36)
even_numbers.append(46)
even_numbers.append(40)
even_numbers.append(42)


# negative indices
print(f'Last element of even_numbers array is: {even_numbers[-1]} and index is: {-1} (Better way)')
print(even_numbers)
# length
print(f'Number of elements in even_numbers array is: {len(even_numbers)}')


# changing elements
even_numbers[0] = 8
even_numbers[3] = 4
even_numbers[5] = 10
even_numbers[-1] = 8

print(even_numbers)


# removing elements from the list
even_numbers.remove(8)
even_numbers.remove(8)
even_numbers.remove(8)
# even_numbers.remove(8)

print(even_numbers)

even_numbers.pop()
even_numbers.pop(5)
# even_numbers.clear()

# sorting
# sort numbers descending is a permutation (reordering) such that
# every number's next number is less than or equal to it
even_numbers.append(46)
even_numbers.sort(reverse=True) # reverse is a keyword argument
print(even_numbers)


# names = ['josh', 'John', 'mary', 'Gio', 'Luka', 'aa', 'AAA']
# name_1 = [106, 111, 115, 104]
# names.sort()
# print(names)

string_list = [
    'AB', 'Aa'
    #66 ,  97
]

string_list.sort()
print(string_list)

