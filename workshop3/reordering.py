numbers = [1, -2, -3, 4, 5]
print(numbers)
# numbers.sort()
# sorted does not affect original object
sorted_numbers = sorted(numbers)
print(f'Sorted: {sorted_numbers}')
print(numbers)

# reverse
numbers.reverse()
print(numbers)

reversed_numbers = list(reversed(numbers))
print(reversed_numbers)

numbers = []
print(numbers, type(numbers))

numbers = list()
print(numbers, type(numbers))

