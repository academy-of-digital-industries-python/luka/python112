first_names = ["John", "Jane", "Jack"]

# Access
print('for index to length of list')
for i in range(len(first_names)):
    message = f"Hello {first_names[i]}"
    print(message)

print('\nfor element in list')
for first_name in first_names:
    print(f"Hello {first_name}")

print('\nfor i, element in enumeration of list')
for i, first_name in enumerate(first_names):
    print(f'{first_name} at index of {i}')



numbers = []
for number in range(1, 20):
    numbers.append(number)

print(numbers)

numbers_to_remove = [13, 15, 6, 5, 1]
numbers_to_remove.sort(reverse=True)
# for number in numbers_to_remove:
#     numbers.remove(number)

for number in numbers_to_remove:
    # iteration
    numbers.pop(number - 1)
    print(numbers)

print(numbers)


"""
We have a list of even numbers, someone messed up 
and we ended up with odd numbers in our list

- remove odd numbers from the list
"""
even_numbers = [2, 9, 23, 12, 210, 19, 23]
