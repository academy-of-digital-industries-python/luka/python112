"""
Data Type: Boolean / bool

values: True / False

Comparison Operators:
== - equal to
!= - not equal to
> - greater than
< - less than
>= - greater than or equal to
<= - less than or equal to

Logical Operators:
and - both operands are True
or - at least one operand is True
not - negates the operand
"""
is_even = 2 % 2 == 0
print(f"{is_even = }", type(is_even))

# print(2 % 2 == 0)

print(f"{(25 * 2) + 6 - 9 * 2 + 12 == 7 * 9 / 3 = }")
# print(f'2 == 2 = {2 == 2}')
print(f"{2 == 2 = }")
print(f"2 != 2 = {2 != 2}")
print(f"{2 > 2 = }")
print(f"{2 < 2 = }")
print(f"{2 >= 2 = }")
print(f"{2 <= 2 = }")

# Logical AND
print("\nLogical AND")
print(f"{True and True = }")
print(f"{True and False = }")
print(f"{False and True = }")
print(f"{False and False = }")

# Logical OR
print("\nLogical OR")
print(f"{True or True = }")
print(f"{True or False = }")
print(f"{False or True = }")
print(f"{False or False = }")

# Logical NOT
print("\nLogical NOT")
print(f"{not True = }")
print(f"{not False = }")

# Logical AND and OR
print("\nLogical AND and OR")
print(f"{True and True or False = }")

# Logical AND and NOT
print("\nLogical AND and NOT")
print(f"{not (True and (False or True)) = }")

print(f"{2 > 2 and 2 < 2 = }")

print(f"{'Giorgi' == 'Giorgi' = }")
print(f"{'Giorgi' == 'giorgi' = }")
print(f"{'Giorgi' != 'giorgi' = }")
print(f"{'GiOrgi' < 'Giorgi' = }")

we_went_to_the_beach = False
we_went_to_the_mall = False

we_did_not_lie = (
    (not we_went_to_the_beach and we_went_to_the_mall)
    or (we_went_to_the_beach and not we_went_to_the_mall)
)

we_did_lie = (
    (we_went_to_the_beach and we_went_to_the_mall)
    or not (we_went_to_the_beach or we_went_to_the_mall)
)
print('\nLie test:')
print(f'{we_went_to_the_beach = }, {we_went_to_the_mall = }')
print(f'{we_did_not_lie = }')
print(f'{we_did_lie = }')

# Truthy and Falsy values

# Type Casting to bool
print('\nType Casting to bool (numbers)')
print(f'{bool(0) = }')
print(f'{bool(1) = }')
print(f'{bool(-1) = }')
print(f'{bool(1000.1) = }')
print(f'{bool(-1000) = }')

print('\nType Casting to bool (strings)')
print(f'{bool("") = }')
print(f'{bool(" ") = }')
print(f'{bool("Hello") = }')
print(f'{bool("False") = }')
print(f'{bool("True") = }')

print('\nType Casting to bool (lists)')
print(f'{bool([]) = }')
print(f'{bool([1]) = }')

print('\nType Casting bool to string and numbers')
print(f'{str(True) = }')
print(f'{str(False) = }')
print(f'{int(True) = }')
print(f'{int(False) = }')
print(f'{float(True) = }')
print(f'{float(False) = }')
