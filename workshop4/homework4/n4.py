# N4
import time
import sys

numbers_list = []
start = time.time()
# print(sys.getsizeof(numbers_list))
# for num in range(1, 1_000_000):
#     numbers_list.append(num)
# list comprehension
# numbers_list = [
#     num for num in range(1, 1_000_000)
# ]
numbers_list = list(range(1, 1_000_000))

print(sys.getsizeof(numbers_list) / 1000 / 1000)
print(f'List has been created, (took: {time.time() - start})')
# for num in numbers_list:
#     print(num)
