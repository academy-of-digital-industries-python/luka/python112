numbers = list(range(1, 6))

print('AVG (func):', sum(numbers) / len(numbers))

i = 0
sum_of_nums = 0
while i < len(numbers):
    sum_of_nums += numbers[i]
    i += 1

print('AVG (while):', sum_of_nums / len(numbers))

sum_of_nums = 0
for num in numbers:
    sum_of_nums += num
print('AVG (for):', sum_of_nums / len(numbers))
