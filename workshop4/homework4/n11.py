pizzas = ['Pepperoni', 'Hawaiian', 'Meat Lovers', 'Veggie', 'Cheese']

# shallow copy
# friends_pizzas = pizzas.copy()
friends_pizzas = pizzas[:]

pizzas.append('Juja')
friends_pizzas.append('Sweet')

# print(id(pizzas))
# print(id(friends_pizzas))
print('Are they equal?', pizzas == friends_pizzas)
# print('Are they same list?', id(pizzas) == id(friends_pizzas))
print('Are they same list?', pizzas is friends_pizzas)

print('My favorite pizzas:', pizzas)
print('My friend\'s favorite pizzas:', friends_pizzas)
