# N5
import time
numbers_list = list(range(1, 1_000_001))

s = time.time()
min_num = min(numbers_list)
min_time = time.time() - s

s = time.time()
max_num = max(numbers_list)
max_time = time.time() - s

s = time.time()
sum_of_nums = sum(numbers_list)
sum_time = time.time() - s

print(f'Min: {min_num} | {min_time}s')
print(f'Max: {max_num} | {max_time}s')
print(f'Sum: {sum_of_nums} | {sum_time}s')

