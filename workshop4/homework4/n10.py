numbers_list = list(range(1, 11))

# 0, 1, 2
print('First three numbers:')
# for i in range(3):
#     print(numbers_list[i])

for number in numbers_list[:3]:
    print(number)

middle_index = (len(numbers_list) // 2) -1
print('Middle three numbers:')
for number in numbers_list[middle_index:middle_index + 3]:
    print(number)

print('Last three numbers:')
for number in numbers_list[-3:]:
    print(number)

# 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

