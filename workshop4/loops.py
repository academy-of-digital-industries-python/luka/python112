"""
Loops

Assignment operators:
= - assign
+= - add and assign
-= - subtract and assign
*= - multiply and assign
/= - divide and assign
//= - floor divide and assign
%= - modulo and assign
"""

# გვინდა რომ დავბეჭდოთ რიცხვები 1-დან 10-მდე
# print(1)
# print(2)
# print(3)
# print(4)
x = 1 # initialization
# While Loop
while x < 10: # condition
    # code block / code fragment / body that will be executed while condition is True
    print(x)
    # x = x + 1
    x += 1  # increment

# For Loop
print('\nFor Loop:')
for i in range(1, 10):
    print(i)
    print('Hello')
    print('World')
    print('!')

