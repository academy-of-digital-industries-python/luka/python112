"""
1. Undo mechanism
2. replay
"""
# type annotations / hinting
# game_range = (1, 100)

lower_bound: int = 1
upper_bound: int = 100
changes: list[tuple[int, int]] = []

chosen_number: int = int(input(f'choose a number ({lower_bound} - {upper_bound}): '))
if chosen_number < lower_bound or upper_bound < chosen_number:
    print('Sorry our number is out of bounds')
    quit()

while True:
    guess = (lower_bound + upper_bound) // 2
    print(f'Computer: {guess}')
    answer: str = input('is it ( lower / higher / correct / undo ) ? ').lower()

    if answer == 'higher':
        changes.append((lower_bound, upper_bound))
        lower_bound = guess
    elif answer == 'lower':
        changes.append((lower_bound, upper_bound))
        upper_bound = guess
    elif answer == 'correct':
        print('Thank you for playing with me!')
        break
    elif answer == 'undo':
        if not changes:
            print('There is nothing to undo!')
            continue
        lower_bound, upper_bound = changes.pop()
    else:
        print('Please enter valid statement!')
