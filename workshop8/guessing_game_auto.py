import random

lower_bound: int = 1
upper_bound: int = 100
chosen_number: int = random.randint(lower_bound, upper_bound)
steps: int = 0
guesses = []

def take_a_guess() -> int:
    while True:
        guess = random.randint(lower_bound, upper_bound)

        if guess not in guesses:
            guesses.append(guess)
            return guess


while True:
    guess: int = (lower_bound + upper_bound) // 2
    # guess = take_a_guess()
    steps += 1

    if guess < chosen_number:
        lower_bound = guess
    elif guess > chosen_number:
        upper_bound = guess
    else:
        print(f'Computer guessed the number in {steps} steps;\n{guess = }, {chosen_number = }')
        break
