# ASCII graphics
import time
import os

i = 0
x = 0

while True:
    os.system('cls')  # syscall
    offset = ' ' * x

    if i % 2:
        print(offset + ' O ')
        print(offset + '/|\\')
        print(offset + '/ \\')
    else:
        x += 1
        print(offset + ' O ')
        print(offset + '///')
        print(offset + '/ /')
    i += 1
    time.sleep(0.5)
