PI: float = 3.14  # constant / const
b: int = 7

def func(a: int = 5) -> int:
    global b
    # function with side effect
    # b = 8 # shadowing
    b += 1
    print(f'Local {b = }')
    return a * b - PI

def bunc():
    print(f'I also have an access to {PI = } and {b =}')

print(f'Global {b = }')
func()
bunc()
print(f'Global {b = }')
