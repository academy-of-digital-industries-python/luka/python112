# Global Scope
def algorithm1(a = 1, b = 1, c = 1, d = 1) -> float:
    # Local Scope
    result = (a ** 2 + b) / (c * d)
    # default value
    return result

# positional arguments / arbitrary
a = 1
b = 2
result = algorithm1(a, b, 2, 2)
print(result)
# equivalent functions
# print(algorithm1(b=2, a=2, d=2, c=2))
print(algorithm1(2, b=2, c=2))
print(algorithm1(2, 2, c=2, d=2))
print(algorithm1())


