class Car:
    brand: str
    model: str

    def __init__(self, brand: str, model: str) -> None:
        self.brand = brand
        self.model = model

    def __repr__(self) -> str:
        return f"Car({self.brand}, {self.model})"


cars: list[Car] = []

with open(r"D:\Workspace\Academy\python112\workshop10\cars.txt") as f:
    for line in f.readlines():
        # print(repr(line))
        # print(line.split(','))
        brand, model = line.strip().split(",")
        # print(brand, model)
        cars.append(Car(brand, model))

print('\nUsing For loop: ')
for car in cars:
    print(car)

print('\nDirectly list: ')
print(cars)
