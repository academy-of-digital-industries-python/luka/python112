from bank import Bank

# print(Bank.name, Bank.address, Bank.value)
tbc = Bank('TBC', 'Marjanishvili', 130_000_000)
tbc.transactions.append(500_000)
tbc.transactions.append(700_000)
tbc.transactions.append(100_000)

bog = Bank('Bank of Georgia', 'Left Bank', 130_000_000)
nyc = Bank('NYC', 'NYC', 1_000_000_000)

# print(tbc.name, tbc.address, tbc.value)
# print(tbc.transactions)
print(str(tbc))
print(tbc.describe())

# print(bog.name, bog.address, bog.value)
# print(bog.transactions)

# print(f'There are {Bank.bank_count} banks in total')