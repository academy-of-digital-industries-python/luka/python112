room: list[str] = [
    '' for _ in range(10)
]

save_file = 'pos.txt'
try:
    with open(save_file) as f:
        pos = f.read()
        position = int(pos)
except Exception:
    position = 0

# print(room)
while True:
    room[position] = '🙂'
    print(room)

    move = input('Direction (l / r / q): ')

    if move == 'l':
        if position - 1 >= 0:
            room[position] = ''
            position -= 1
    elif move == 'r':
        if position + 1 < len(room):
            room[position] = ''
            position += 1
    elif move == 'q':
        break

with open(save_file, 'w') as f:
    f.write(str(position))

    