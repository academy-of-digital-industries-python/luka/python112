class Account:
    iban: str
    balance: float

    def __init__(self, iban: str, balance: float = 0) -> None:
        self.iban = iban
        self.balance = balance
    
    def update_balance(self, amount: float) -> None:
        if amount < 0 and self.balance < abs(amount):
            raise ValueError('Insufficient fund on balance!')
        self.balance += amount

    def __str__(self) -> str:
        return self.iban

    def __repr__(self) -> str:
        return str(self)


class Transaction:
    from_account: Account
    to_account: Account
    amount: float

    def __init__(
        self, from_account: Account, to_account: Account, amount: float
    ) -> None:
        self.from_account = from_account
        self.to_account = to_account
        self.amount = amount
    
    def is_valid(self) -> bool:
        try:
            self.from_account.update_balance(-self.amount)
            self.to_account.update_balance(self.amount)
            return True
        except ValueError:
            return False

    def __str__(self) -> str:
        return (
            f"Transfer from {self.from_account} to {self.to_account}, {self.amount} USD"
        )

    def __repr__(self) -> str:
        return str(self)


class Bank:
    name: str
    address: str
    value: int
    transactions: list[Transaction]

    # static attributes
    bank_count: int = 0

    def __init__(self, name: str, address: str, value: int) -> None:
        self.name = name
        self.address = address
        self.value = value
        self.transactions = []

        Bank.bank_count += 1
        # self.bank_count += 1

    def __str__(self) -> str:
        return self.describe()

    def describe(self) -> str:
        return f"{self.name} - {self.address} - {self.value}"

    def transfer_money(self, from_account: Account, to_account: Account, amount: float) -> None:
        transaction = Transaction(from_account, to_account, amount)
        if transaction.is_valid():
            self.transactions.append(transaction)
        else:
            print('Transaction unsuccessful!')


if __name__ == '__main__':
    a1: Account = Account("1", 100_000)
    a2: Account = Account("2", 0)

    tbc = Bank("TBC", "Marj", 13_000_000)

    # tbc.transactions.append(Transaction(a1, a2, 5000))
    tbc.transfer_money(a1, a2, 10)
    tbc.transfer_money(a2, a1, 5)
    tbc.transfer_money(a2, a1, 5)


    print(tbc.transactions)
    print(a1.balance)
    print(a2.balance)

