#include <iostream>

using namespace std;

class Dog {
    public:
    string name;
    int age;

    Dog(string name, int age) {
        // same as __init__
        this->name = name;
        this->age = age;
    }
};

int main() {
    Dog dog = Dog("Fido", 3);
    cout << dog.name << " " << dog.age << endl;
    return 0;
}