try:
    divisor = int(input('Divisor: '))
    print(5 / divisor)
except ZeroDivisionError:
    print('Cannot divide by 0')
except ValueError:
    print('Please enter valid integer!')
# except (ValueError, ZeroDivisionError):
#     print('Please enter valid integer!')
except Exception:
    print('IDK what happened')
