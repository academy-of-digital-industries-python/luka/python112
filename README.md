# Python112

## Homeworks
- [How To Submit Homeworks](https://youtu.be/jXpT8eOzzCM?si=ZSyXYB9cZhcega1q)

- [Assignment 1](https://classroom.github.com/a/d_EBOQUW)
- [Assignment 2](https://classroom.github.com/a/jGnjkKqw)
- [Assignment 3](https://classroom.github.com/a/VWKEakfM)
- [Assignment 4](https://classroom.github.com/a/MwPdqeOr)
- [Assignment 5](https://classroom.github.com/a/-FsLZxKE)
- [Assignment 6](https://classroom.github.com/a/w3wlNXA2)
- [Assignment 7](https://classroom.github.com/a/9IxdFkbZ)
- [Assignment 8](https://classroom.github.com/a/pbjLDHUm)




## Resources
- [Main Book](https://1drv.ms/b/s!AmZJMrBsKhiOhYRVjF_6FufcwBQI8w?e=xhp31b)
- [Working on projcets](./docs/working_on_projects.MD)
### Extras
- [Data Analysis](https://1drv.ms/b/s!AmZJMrBsKhiOhvFTq-abYD0d2x7mjg?e=9kFOQb)

### Programing Languages (with low level control)
- [C++](https://notalentgeek.github.io/note/note/project/project-independent/pi-brp-beginning-c-programming/document/20170807-1504-cet-1-book-and-source-1.pdf)
- [Rust](https://doc.rust-lang.org/rust-by-example/)