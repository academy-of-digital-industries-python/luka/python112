"""
Single value that is an empty = None (null, none, nil, NIL) 
"""

a = None
print(a, type(a))
