
city = {
    'name': 'Bangkok',
    'population': 13
}
print('\nWhen directly iterating through dict: ')
for key in city:
    # key
    print(key)

print('\nWhen Only keys are needed: ')
for key in city.keys():
    print(key)


print('\nWhen Only values are needed: ')
for value in city.values():
    print(value)

print('\nWhen Both are needed: ')
for key in city:
    print(key, city[key])


print('\nitems method: ')
for key, value in city.items():
    print(key, value)