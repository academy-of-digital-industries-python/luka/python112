import copy
a = {
    'a': 5,
    'b': [1, 2, 3, 4]
}

c = a.copy() # don't use this when you have a data structure inside
a['c'] = a

c = copy.deepcopy(a)
# print(id(c['c']), id(c))

a['b'].append(5)
print(a['c']['c']['c']['c']['b'])
print(c)

print(id(a['b']), id(c['b']))
