country = {
    'name': 'Georgia',
    'population': 3.7,
    'capital': {
        'name': 'Tbilisi',
        'population': 1.7,
    },
    'secondary': {
        'name': 'Kutaisi',
        'population': 0.4
    }
}

print(country['capital']['name'])
# take all key values from every dict in country dict
for key, value in country.items():
    if type(value) == dict:
        for key_2, value_2 in value.items():
            print(key_2, value_2)
