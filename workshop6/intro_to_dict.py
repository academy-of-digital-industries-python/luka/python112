"""
Data Structure: Dictionary, dict
"""
# pet = dict()
pet = {}

pet['type'] = 'bird'
pet['name'] = 'Parry'
pet['age'] = 5


student = {
    # key: value
    "first_name": "John",  # item / pair
    "last_name": "Doe",
    "age": 20,
    "pets": [
        pet,
        {
            "type": "dog",
            "name": "Margaret",
        },
    ],
}

# access

# search by key similar to index
print(student["first_name"])
# KeyError similar to IndexError
# print(student['John'])
print(student["pets"][0]["name"])

# remove
student['pets'][0].pop('age')
student.pop('age')
print(student)

# change
student['first_name'] = 'Josh'
print(student)

# new values
student['age'] = 25
student['car'] = 'Tesla'

print(student)

# accessing with get
last_name = student.get('LastName')
if last_name is None:
    print('LastName is not a valid key')
else:
    print(last_name)

