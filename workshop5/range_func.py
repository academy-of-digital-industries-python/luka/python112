"""
1. range(start, stop, step)
2. range(start, stop)
3. range(stop)
"""

print(list(range(1, 11, 2)))
print(list(range(1, 11)))
print(list(range(11)))
