"""
Control flow

1.
if condition:
    execute this code
    .
    ---

2.
if condition:
    execute this code
    .
    ---
else:
    execute this code
    .
    ---

3.
if condition:
    execute this code
    .
    ---
elif condition:
    execute this code
    .
    ---
elif condition:
    execute this code
elif condition:
    execute this code

    
4.
if condition:
    execute this code
    .
    .
    ---
elif condition:
    execute this code
    .
    ---
elif condition:
    execute this code
elif condition:
    execute this code
else:
    execute this code

"""

age = 3

if age > 18:
    print('Adult')
else:
    print('Minor')

if age > 32:
    print('Welcome')
elif age == 32:
    print('Here is your gift')


if age < 4:
    print('Free')
# elif age > 4 and age < 18:
elif 4 < age < 18:
    print('25$')
else:
    print('40$')
