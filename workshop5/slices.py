"""
slice takes 3 arguments: start, stop, and step
1. [start:stop:step]
2. [start:stop]
3. [start:]
4. [:stop]
5. [::step]
6. [:]
"""

nums = list(range(11))
print(nums)
print(nums[1:-1:2])
print(nums[1:-1])
print(nums[1:])
print(nums[:-1])
print(nums[::2])

# copy
print(nums[:] == nums)
print(id(nums[:]) == id(nums))
print(id(nums[:]))
print(id(nums))
print(nums[:] is nums)
print(nums.copy() is nums)
