available_topping = [
    'mushrooms', 
    'cheese',
    'pepperoni'
]
requested_toppings = []
final_product = 'pizza with '

while True:
    topping = input('Enter Topping: ')
    if topping.lower() == 'done':
        break
    requested_toppings.append(topping)



for requested_topping in requested_toppings:
    if requested_topping.lower() not in available_topping:
        print(f'We do not have {requested_topping}, sorry!')
    else:
        final_product += f'{requested_topping},'

if len(requested_toppings) == 0:
    print('Your pizza will be without anything!')
else:
    print(f'Take your {final_product}')

