"""
variable = [
    value (what should be in the list)
    for value in iterable (what is the source of the values)
]
"""

numbers = [
    number
    for number in range(1, 1001)
]

even_numbers = [
    number
    for number in range(0, 1001, 2)
]

fake_odd_numbers = [
    number * 2
    for number in range(1, 1001, 2)
] 
# print(fake_odd_numbers)
