from django.db import models
from django.utils import timezone


class Post(models.Model):
    # ORM - Object Relational Mapping / Mapper
    author = models.CharField(max_length=255)
    title = models.CharField(max_length=25)
    text = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)

    def is_new(self) -> bool:
        today = timezone.now().date()
        return today == self.date_created.date()
