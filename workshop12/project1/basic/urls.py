from django.urls import path

from basic.views import post_list_view, post_detail_view, post_create_view

urlpatterns = [
    path('posts/', post_list_view, name='post-list'),
    path('posts/<int:post_id>/', post_detail_view, name='post-detail'),
    path('posts/create/', post_create_view, name='post-create')
]
