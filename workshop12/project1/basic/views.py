import random
import string

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpRequest, HttpResponse
from basic.models import Post
from basic.forms import PostCreateForm


# Create your views here.
def home_view(request: HttpRequest) -> HttpResponse:
    context = {
        'title': 'This is variable from python',
        'posts': Post.objects.all()
    }
    # print(context)
    return render(
        request=request,
        template_name='home.html',
        context=context
    )

def post_list_view(request: HttpRequest) -> HttpResponse:
    return render(
        request,
        'posts/post_list.html',
        {
            'posts': Post.objects.order_by('-date_created').all()
        }
    )

def post_detail_view(request: HttpRequest, post_id: int) -> HttpResponse:
    return render(
        request,
        'posts/post_detail.html',
        {
            'post': get_object_or_404(Post, id=post_id)
        }
    )

def post_create_view(request: HttpRequest) -> HttpResponse:
    if request.method == 'GET':
        return render(
            request,
            'posts/post_create.html',
            {
                'form': PostCreateForm()
            }
        )
    print(request.POST)
    form = PostCreateForm(request.POST)

    if form.is_valid():
        post: Post = form.save()
        return redirect('post-detail', post_id=post.id)

    return render(
        request,
        'posts/post_create.html',
        {
            'form': form
        }
    )

def about_view(request: HttpRequest) -> HttpResponse:
    return render(request, 'about.html')


def contact_view(request: HttpRequest) -> HttpResponse:
    return render(request, 'contact.html')
