from django import forms
from basic.models import Post


class PostCreateForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = "__all__"
