# Final Project

Create a console game using only Python. The game should be a simple text-based game. Player should
guess the country name based on the capital city name. The game should have at least 50 countries and their capitals. The game should be able to keep track of the score and display the score at the end of the game. The game should also have a feature to display the correct answer if the player fails to guess the country name.

* if player fails to guess the country name 3 time game should end and display the score.
* You should have file where you store the countries and their capitals. You should read the file and store the data in a dictionary. You should use the dictionary to get the capital name and the country name.
* You should use functions to organize your code. You should have a function to display the score, a function to display the correct answer, a function to display the question, a function to get the user input, and a function to check the user input.
* You should include leaderboard to keep track of the top 10 scores, how much time it took to complete the game, and correct answers. You should display the leaderboard at the end of the game. *(You should store the leaderboard in a file.)*
* break the code into multiple files. You should have a file for the main game, a file for the functions, and a file for the data.
* Player should be able to play the game multiple times without restarting the game.
* Player wins if they guess all the countries correctly.

* You should use the `random` module to shuffle the countries and their capitals.
* **(Bonus)** Use classes to organize your code.
* **(Bonus)** Use the `os` module to clear the screen after each question. [Hint](https://stackoverflow.com/questions/2084508/clear-terminal-in-python)
* **(Bonus)** Use the `time` module to display the time taken to complete the game. [Hint](https://www.geeksforgeeks.org/time-perf_counter-function-in-python/)
