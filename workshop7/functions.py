"""
Functions are code block / fragment
that will be executed when function is called
"""

# function definition
def func(what_should_be_printed):
    # function body / code fragment
    print(what_should_be_printed)

# function call
# print(True)
func('Hello')  # call / invoke


def add(a, b):
    print('I am about to add two numbers: ')
    print(a + b)

def multiply(a: int, b: int) -> int:
    # type hinting
    if not (isinstance(a, int) and isinstance(b, int)):
        return 0
    return a * b

def sub(a, b):
    print(a - b)


def do_the_magic(a, b):  # a, b -> do_magic local area
    print(add(a, b))
    print(multiply(a, b))
    add(a ** 2, b)
    add(b, b)
    add(a, b)


add(65, 2)
add(7, 2)
add(2, 2)
add(8, 2)
sub(0, 99)

c = multiply(2, 25)
print(c)
print(multiply(3, 25))
# print(multiply('25', '25'))

do_the_magic(5, 7)
do_the_magic(9, 7)
do_the_magic(5, 19)
do_the_magic(5, 100)
