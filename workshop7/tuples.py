"""
Data Structure: Tuple / tuple
immutability 
"""
import sys

names = ('Luka', 'Gio', 'John', 'Josh')

print(type(names))
print(names)

# names[0] = 'James'
# access
print(names[0])

# re-declare
names = ('Johnny', 'Jimmy', 'John', 'Josh')

# iterating through
print('\nNormal Loop: ')
for name in names:
    print(name)

print('\nEnumerate Loop: ')
for i, name in enumerate(names):
    print(i, name)

print('\nDifference with lists: ')
names1 = ['Luka', 'Gio', 'John', 'Josh']

print(len(names), sys.getsizeof(names))
print(len(names1), sys.getsizeof(names1))

# defining empty tuples
a = ()
a = tuple()

print(a, type(a))

# not recommended
# do not create tuples without braces
# a = 5, 6, 7, 8, 9
a = (5, 6, 7, 8, 9)
print(a, type(a))