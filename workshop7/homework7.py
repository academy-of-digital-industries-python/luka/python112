"""
Implement a function that checks a text and returns corrected text. there is a list of words 
(that is taken from file english_words.txt, you can ignore this part), first of all implement 
a function that finds closest word in words list to passed word you have to define a function called 
find_closest that will take 1 parameter and return 1 closest word to it:

word - find the closest word to this word from words list, use similarity function to determine how similar are words
similarity function returns percentage of 2 strings, how similar are they.

then implement spell_check function that will correct the text and return corrected text.

NOTES

punctuation will not be included!
words will be only in english
"""

import difflib
from pathlib import Path

# IGNORE
BASE_DIR = Path(__file__).resolve().parent
with open(BASE_DIR / 'english_words.txt') as f:
    words: list[str] = f.read().split('\n')
    


def similarity(str1, str2) -> float:
    return difflib.SequenceMatcher(None, str1, str2).ratio() * 100


# IGNORE


# WORK HERE
def find_closest(word: str) -> str:
    closest = 0
    closest_word = word
    for word_ in words:
        similarity_ratio = similarity(word, word_)
        if similarity_ratio > closest:
            closest_word = word_
            closest = similarity_ratio
    
    return closest_word

def spell_check(text: str) -> str:
    new_text = []
    for word in text.split(' '):
        new_text.append(find_closest(word))
    
    return ' '.join(new_text)


coorect_text = spell_check('tonigh wille be realy goood ')  # tonight was really good
print(coorect_text)
