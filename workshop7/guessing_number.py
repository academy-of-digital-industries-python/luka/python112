import random

# generate random number and save it in memory
random_number = random.randint(0, 100)
# random_number = 11

def take_user_input():
    while True:
        guess = input('Take a guess: ')
        # print(repr(guess))
        if not guess.isdigit():
            # Guard Clause
            print('Please input only integers!')
            continue

        return int(guess)

# ask user indefinitely for number to guess
is_running = True
count = 0
while is_running:
    guess = take_user_input()
    if random_number < guess:
        print('Lower')
    elif random_number > guess:
        print('Greater')
    else:
        print('You Guessed the number correctly!')
        # is_running = False
        break
    count += 1
    print(f'You tried {count} times!')

    if count >= 10:
        print('You lost the game!')

    # print('End of the loop')

# best case 50 (1 step)
# worst case 1 (7 step)
# log n steps
