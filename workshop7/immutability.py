"""
str and tuples are immutable
"""

a = (
    [1, 2, 3, 4],
    ['1', '2', '3'],
    {'first_name': 'Josh', 'age': 25},
    (1, 2, 3),
    0,
    True,
    'String',
    None
)

a[0].append(25)
a[1].pop()
# a[1][0][0] = 'L'
a[2]['first_name'] = 'George'
# a[3][1] = 5
# a[4] += 17
# a[6][3] = 'J'

print(a, type(a))

name = 'josh'
name += ' + help'
print(name)
